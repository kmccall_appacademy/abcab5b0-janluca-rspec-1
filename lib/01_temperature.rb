def ftoc(temp)
  ((temp - 32.0) * 0.5556).to_i
end

def ctof(temp)
  ((temp * 1.8) + 32.0).to_f.round(1)
end
