def translate(words_str)
  words = words_str.split(" ")
  words.map! do |word|
    pigify_word(word)
  end

  words.join(" ")
end

def pigify_word(word)
  letters = word.split("")
  i = 0

  if is_vowel?(letters[0])
    return letters.join("") + "ay"
  else
    while !is_vowel?(letters[i])
      if letters[i] == "q" && letters[i + 1] == "u"
        i += 2
      else
        i += 1
      end
    end
  end

  before_vow = letters.slice(0, i).join("")
  at_vow = letters.slice(i, letters.length).join("")

  at_vow + before_vow + "ay"
end

def is_vowel?(ltr)
  vow = ["a", "e", "i", "o", "u"]
  if vow.include? ltr
    return true
  end
  false
end
