def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(nums)
  if nums.empty?
    return 0
  end
  nums.reduce(:+)
end

def multiply(nums)
  nums.reduce(:*)
end

def power(num, pow)
  num**pow
end

def factorial(num)
  if num < 1
    return 0
  end
  nums = [*1..num]
  nums.reduce(:*)
end
