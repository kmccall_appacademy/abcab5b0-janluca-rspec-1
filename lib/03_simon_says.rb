require 'byebug'

def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, n = 2)
  Array.new(n, str).join(" ")
end

def start_of_word(str, n)
  str[0, n]
end

def first_word(str)
  str.split(" ")[0]
end

def titleize(word_str)
  words = word_str.split(" ")
  little = ["and", "the", "over"]
  words.map do |wordy|
    if !little.include? wordy
      wordy.capitalize!
    end
  end
  words[0] = words[0].capitalize
  words.join(" ")
end
